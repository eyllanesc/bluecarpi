from bluetooth import *
from Car import Car

import os

os.system("sudo hciconfig hci0 name \'raspberrypi-0\'")

os.system("sudo hciconfig hci0 piscan")

server_sock = BluetoothSocket(RFCOMM)
server_sock.bind(("", PORT_ANY))
server_sock.listen(1)

port = server_sock.getsockname()[1]

uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

advertise_service(server_sock, "AquaPiServer",
                  service_id=uuid,
                  service_classes=[uuid, SERIAL_PORT_CLASS],
                  profiles=[SERIAL_PORT_PROFILE],
                  #                   protocols = [ OBEX_UUID ]
                  )
motorL = [17, 27]
motorR = [23, 24]

car = Car(motorL, motorR)

while True:
    print "Waiting for connection on RFCOMM channel %d" % port

    client_sock, client_info = server_sock.accept()
    print "Accepted connection from ", client_info

    try:
        data = client_sock.recv(1024)
        if len(data) == 0:
            break

        if data == 'Forward':
            car.forward()
        elif data == 'Backward':
            car.backward()
        elif data == 'Left':
            car.left()
        elif data == 'Right':
            car.right()
        elif data == 'Stop':
            car.stop()
        else:
            data = 'ERROR'
        data += "!"
        client_sock.send(data)
    except IOError:
        pass

    except KeyboardInterrupt:
        client_sock.close()
        server_sock.close()
        break
