package com.rpi.bluecarpi.bluecarpi;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    BluetoothSocket _Socket;
    BluetoothDevice _Device = null;

    final byte delimiter = 33;
    int readBufferPosition = 0;

    public void sendBtMsg(String msg){
        UUID uuid = UUID.fromString("94f39d29-7d6d-437d-973b-fba39e49d4ee"); //Standard SerialPortService ID
        try {

            _Socket = _Device.createRfcommSocketToServiceRecord(uuid);
            if (!_Socket.isConnected()){
                _Socket.connect();
            }

            OutputStream mmOutputStream = _Socket.getOutputStream();
            mmOutputStream.write(msg.getBytes());

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Handler handler = new Handler();

        final Button btnBackward = (Button)findViewById(R.id.btnBackward);
        final Button btnForward = (Button)findViewById(R.id.btnForward);
        final Button btnLeft = (Button)findViewById(R.id.btnLeft);
        final Button btnRight = (Button)findViewById(R.id.btnRight);
        final Button btnStop = (Button)findViewById(R.id.btnStop);

        final BluetoothAdapter _bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        final class workerThread implements Runnable{

            private String btMsg;

            public workerThread(String msg){
                btMsg = msg;
            }

            @Override
            public void run() {
                sendBtMsg(btMsg);

                while (!Thread.currentThread().isInterrupted()){
                    int bytesAvailable;

                    boolean workDone = false;
                    try {

                        final InputStream _inputStream;
                        _inputStream = _Socket.getInputStream();

                        bytesAvailable = _inputStream.available();

                        if(bytesAvailable>0){
                            byte[] packetBytes = new byte[bytesAvailable];
                            byte[] readBuffer = new byte[1024];
                            _inputStream.read(packetBytes);

                            for(int i=0; i<bytesAvailable; i++){
                                byte b=packetBytes[i];
                                if(b==delimiter){
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {

                                        }
                                    });
                                    workDone = true;
                                    break;
                                }
                                else {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }

                            if(workDone){
                                _Socket.close();
                                break;
                            }
                        }
                    }
                    catch (IOException e){
                        e.printStackTrace();
                    }
                }

            }
        }


        assert btnForward != null;
        btnForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (new Thread(new workerThread("Forward"))).start();
            }
        });

        assert btnBackward != null;
        btnBackward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (new Thread(new workerThread("Backward"))).start();
            }
        });

        assert btnLeft != null;
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (new Thread(new workerThread("Left"))).start();
            }
        });

        assert btnRight != null;
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (new Thread(new workerThread("Right"))).start();
            }
        });

        assert btnStop != null;
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (new Thread(new workerThread("Stop"))).start();
            }
        });

        if(!_bluetoothAdapter.isEnabled()){
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetooth, 0);
        }

        Set<BluetoothDevice> pairedDevice = _bluetoothAdapter.getBondedDevices();

        if(pairedDevice.size() > 0){
            for (BluetoothDevice device:pairedDevice){
                if(device.getName().equals("raspberrypi-0")){
                    _Device = device;
                }
            }
        }
    }


}
